﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseUserEntity : BaseEntity
    {
        protected BaseUserEntity()
        {
        }

        protected BaseUserEntity(string firstName, string lastName, string email)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
        }
        
        [MaxLength(50)]
        public string FirstName { get; set; }
        
        [MaxLength(50)]
        public string LastName { get; set; }
        public string FullName => $"{FirstName} {LastName}";
        
        [MaxLength(50)]
        public string Email { get; set; }
    }
}