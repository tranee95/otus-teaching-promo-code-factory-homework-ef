﻿namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
    }
}