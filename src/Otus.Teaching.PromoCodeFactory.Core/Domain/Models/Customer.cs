﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Models
{
    public class Customer : BaseUserEntity
    {
        public Customer()
        {
        }

        public Customer(string firstName, string lastName, string email) : base
            (firstName, lastName, email)
        {
        }

        public Guid PromoCodId { get; set; }
        public virtual List<PromoCode> PromoCodes { get; set; }
        public virtual List<CustomerPreference> CustomerPreferences { get; set; }
    }
}