﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }

        public void GeneratedId() => Id = Guid.NewGuid();
    }
}