﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.Customer;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]/[action]")]
    public class CustomerController : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public CustomerController(
            IRepository<Customer> customerRepository, 
            IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
        }
        
        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetAllCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var result = customers.Select(x => new CustomerShortResponse(x)).ToList();

            return result;
        }
        
        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomerByIdAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();
            
            return new CustomerShortResponse(customer);;
        }
        
        /// <summary>
        /// Получить предпочтения клиента по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Preference>>> GetCustomerPreferenceById(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();
            
            return customer.CustomerPreferences.Select(s => s.Preference).ToList();;
        }

        /// <summary>
        /// Создание клиента
        /// </summary>
        /// <param name="customerItem"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<CustomerItemResponse>> Create([FromBody] CustomerItemResponse customerItem)
        {
            var customer = new Customer(customerItem.FirstName,
                customerItem.LastName,
                customerItem.Email);
            
            var result = await _customerRepository.AddAsync(customer);
            return new CustomerItemResponse(result);
        }

        /// <summary>
        /// Изменение данных клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="customerItem"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<CustomerShortResponse>> Update(Guid id, CustomerItemResponse customerItem)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer is null) return NotFound();

            customer.FirstName = customerItem.FirstName;
            customer.LastName = customerItem.LastName;
            customer.Email = customer.Email;
            
            await _customerRepository.UpdateAsync(customer);
            return new CustomerShortResponse(customer);
        }
        
        /// <summary>
        /// Удаление клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult<bool>> Delete(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var result = await _customerRepository.DeleteAsync(customer);

            foreach (var promoCode in customer.PromoCodes)
            {
                await _promoCodeRepository.DeleteAsync(promoCode);
            }
            
            if (!result) return NotFound();

            return Ok();
        }
    }
}