﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>
        {
            new Employee
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            }
        };

        public static IEnumerable<Role> Roles => new List<Role>
        {
            new Role
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор"
            },
            new Role
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };

        public static IEnumerable<Customer> Customers => new List<Customer>
        {
            new Customer()
            {
                Id = Guid.Parse("97a1f1ca-c7f3-450d-a27d-97b896d24566"),
                Email = "ivan_sergeev@mail.ru",
                FirstName = "Семен",
                LastName = "Иванов",
                PromoCodId = Guid.Parse("dbb65298-a697-42b4-8ecb-52b471f0c47b"),
                CustomerPreferences = new List<CustomerPreference>()
                {
                    new CustomerPreference()
                    {
                        CustomerId = Guid.Parse("97a1f1ca-c7f3-450d-a27d-97b896d24566"),
                        PreferenceId = Guid.Parse("70747d1b-ffd1-4a9d-9322-607e615bb7b4")
                    },
                    new CustomerPreference()
                    {
                        CustomerId = Guid.Parse("97a1f1ca-c7f3-450d-a27d-97b896d24566"),
                        PreferenceId = Guid.Parse("c9c0a8a4-abef-4655-b87a-02a513d78767")
                    }
                }
            }
        };

        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode
            {
                Id = Guid.Parse("dbb65298-a697-42b4-8ecb-52b471f0c47b"),
                ServiceInfo = "ServiceInfo",
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(1),
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("70747d1b-ffd1-4a9d-9322-607e615bb7b4"),
                Name = "CustomerPreferences 1",
            },
            new Preference()
            {
                Id = Guid.Parse("c9c0a8a4-abef-4655-b87a-02a513d78767"),
                Name = "CustomerPreferences 2",
            },
            new Preference()
            {
                Id = Guid.Parse("7cc5e3db-d363-4db9-9af3-53ae182f48f8"),
                Name = "CustomerPreferences 3",
            }
        };
    }
}